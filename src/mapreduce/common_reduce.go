package mapreduce

import (
	"encoding/json"
	"log"
	"os"
	"sort"
)

// doReduce does the job of a reduce worker: it reads the intermediate
// key/value pairs (produced by the map phase) for this task, sorts the
// intermediate key/value pairs by key, calls the user-defined reduce function
// (reduceF) for each key, and writes the output to disk.
type KVs []KeyValue

func (p KVs) Len() int {
	return len(p)
}
func (p KVs) Less(i, j int) bool {
	return p[i].Key < p[j].Key
}
func (p KVs) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
func doReduce(
	jobName string, // the name of the whole MapReduce job
	reduceTaskNumber int, // which reduce task this is
	nMap int, // the number of map tasks that were run ("M" in the paper)
	reduceF func(key string, values []string) string,
) {
	// TODO:
	// You will need to write this function.
	// You can find the intermediate file for this reduce task from map task number
	// m using reduceName(jobName, m, reduceTaskNumber).
	// Remember that you've encoded the values in the intermediate files, so you
	// will need to decode them. If you chose to use JSON, you can read out
	// multiple decoded values by creating a decoder, and then repeatedly calling
	// .Decode() on it until Decode() returns an error.
	//
	// You should write the reduced output in as JSON encoded KeyValue
	// objects to a file named mergeName(jobName, reduceTaskNumber). We require
	// you to use JSON here because that is what the merger than combines the
	// output from all the reduce tasks expects. There is nothing "special" about
	// JSON -- it is just the marshalling format we chose to use. It will look
	// something like this:
	//
	// enc := json.NewEncoder(mergeFile)
	// for key in ... {
	// 	enc.Encode(KeyValue{key, reduceF(...)})
	// }
	// file.Close()

	outfile, err := os.Create(mergeName(jobName, reduceTaskNumber))
	if err != nil {
		log.Fatal(err)
	}
	defer outfile.Close()

	enc := json.NewEncoder(outfile)
	var kv KVs
	for i := 0; i < nMap; i++ {
		infile, err := os.Open(reduceName(jobName, i, reduceTaskNumber))
		if err != nil {
			log.Fatal(err)
		}
		defer infile.Close()
		dec := json.NewDecoder(infile)
		var tmp KeyValue
		for dec.Decode(&tmp) == nil {
			kv = append(kv, tmp)
		}
	}
	sort.Sort(kv)
	var con []string
	for i := 0; i < len(kv); i++ {
		con = append(con, kv[i].Value)
		if i == len(kv)-1 || (kv[i].Key != kv[i+1].Key) {
			enc.Encode(KeyValue{kv[i].Key, reduceF(kv[i].Key, con)})
			con = []string{}
		}
	}
}
