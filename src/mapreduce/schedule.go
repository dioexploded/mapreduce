package mapreduce

import (
	"fmt"
	"sync"
)

type laji struct {
	tknm string
	tkno int
}
type task struct {
	sync.Mutex
	count int
}

// schedule starts and waits for all tasks in the given phase (Map or Reduce).
func (mr *Master) schedule(phase jobPhase) {
	var ntasks int
	var nios int // number of inputs (for reduce) or outputs (for map)
	switch phase {
	case mapPhase:
		ntasks = len(mr.files)
		nios = mr.nReduce
	case reducePhase:
		ntasks = mr.nReduce
		nios = len(mr.files)
	}
	type empty struct{}
	var naive empty
	fmt.Printf("Schedule: %v %v tasks (%d I/Os)\n", ntasks, phase, nios)
	tch := make(chan laji, ntasks)
	for i := 0; i < ntasks; i++ {
		tch <- laji{mr.files[i], i}
	}
	count := 0
	var zzk sync.Mutex
	for {
		wknm := <-mr.registerChannel
		tk, ok := <-tch
		if !ok {
			break
		}
		go func(tk laji, wknm string) {
			flag := call(wknm, "Worker.DoTask", &DoTaskArgs{mr.jobName, tk.tknm, phase, tk.tkno, nios}, &naive)
			if flag {
				zzk.Lock()
				count++
				if count == ntasks {
					close(tch)
				}
				zzk.Unlock()
			} else {
				tch <- tk
			}
			mr.registerChannel <- wknm
		}(tk, wknm)
	}
	// All ntasks tasks have to be scheduled on workers, and only once all of
	// them have been completed successfully should the function return.
	// Remember that workers may fail, and that any given worker may finish
	// multiple tasks.
	//
	// TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
	//

	fmt.Printf("Schedule: %v phase done\n", phase)
}
